package main

import (
	"context"
	"database/sql"
	"fmt"
	"math"
	"math/rand"
	"time"
)

type EmployeeRepository interface {
	Fetch(ctx context.Context, limit int) ([]Employee, error)
}

func NewEmployeeRepository(db *sql.DB) EmployeeRepository {
	return &employeeRepositoryMysql{db: db}
}

type employeeRepositoryMysql struct {
	db *sql.DB
}

func (r *employeeRepositoryMysql) Fetch(ctx context.Context, limit int) ([]Employee, error) {
	stmt, err := r.db.PrepareContext(ctx, `
SELECT
 e.emp_no,
 e.first_name,
 e.last_name,
 (SELECT salary FROM salaries WHERE salaries.emp_no = e.emp_no ORDER BY salary DESC LIMIT 1) as salary,
 (SELECT from_date FROM salaries WHERE salaries.emp_no = e.emp_no ORDER BY salary DESC LIMIT 1) as salary_date
FROM employees AS e
ORDER BY hire_date, gender
LIMIT ?
`)
	if err != nil {
		return nil, err
	}

	defer stmt.Close()

	rows, err := stmt.QueryContext(ctx, limit)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	result := make([]Employee, 0)

	for rows.Next() {
		var e Employee

		err = rows.Scan(&e.ID, &e.FirstName, &e.LastName, &e.MaxSalary, &e.SalaryDate)
		if err != nil {
			return nil, err
		}

		result = append(result, e)
	}

	return result, nil
}

func WrapWithCached(repo EmployeeRepository, c *cache) EmployeeRepository {
	return &salaryRepositoryCached{salaries: repo, cache: c, beta: 1}
}

type salaryRepositoryCached struct {
	salaries EmployeeRepository
	cache    *cache
	beta     float64
}

func (r *salaryRepositoryCached) Fetch(ctx context.Context, limit int) ([]Employee, error) {
	key := r.makeKey(limit)

	val, delta, expiry := r.cache.Get(key)
	if val == nil || r.shouldRenew(delta, expiry) {
		start := time.Now()

		salaries, err := r.salaries.Fetch(ctx, limit)
		if err != nil {
			return nil, err
		}

		delta := time.Since(start)

		r.cache.Set(key, salaries, delta, time.Second*20)

		val = salaries
	}

	return val.([]Employee), nil
}

func (r *salaryRepositoryCached) shouldRenew(delta time.Duration, expiry time.Time) bool {
	factor := math.Log(rand.Float64())

	return expiry.Before(time.Now().Add(time.Duration(-1 * (float64(delta) * r.beta * factor))))
}

func (r *salaryRepositoryCached) makeKey(limit int) string {
	return fmt.Sprintf("%d", limit)
}
