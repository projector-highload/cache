package main

import (
	"sync"
	"time"
)

func NewCache() *cache {
	return &cache{RWMutex: &sync.RWMutex{}, store: make(map[string]cacheItem)}
}

type cacheItem struct {
	val    interface{}
	delta  time.Duration
	expiry time.Time
}

type cache struct {
	*sync.RWMutex
	store map[string]cacheItem
}

func (c *cache) Set(key string, value interface{}, delta, ttl time.Duration) {
	c.Lock()
	defer c.Unlock()

	c.store[key] = cacheItem{val: value, delta: delta, expiry: time.Now().Add(ttl)}
}

func (c *cache) Get(key string) (value interface{}, delta time.Duration, expiry time.Time) {
	c.RLock()
	defer c.RUnlock()

	now := time.Now()

	item, ok := c.store[key]
	if !ok || item.expiry.Before(now) {
		return nil, 0, item.expiry
	}

	return item.val, item.delta, item.expiry
}
