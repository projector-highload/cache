URL=http://127.0.0.1:9090/fetch
URL_CACHE="${URL}?cache=1"

10:
	siege -c 10 -b -t 1m --no-parser ${URL}

10c:
	siege -c 10 -b -t 1m --no-parser "${URL_CACHE}"

25:
	siege -c 25 -b -t 1m --no-parser "${URL}"

25c:
	siege -c 25 -b -t 1m --no-parser "${URL_CACHE}"

50:
	siege -c 50 -b -t 1m --no-parser "${URL}"

50c:
	siege -c 50 -b -t 1m --no-parser "${URL_CACHE}"

100:
	siege -c 100 -b -t 1m --no-parser "${URL}"

100c:
	siege -c 100 -b -t 1m --no-parser "${URL_CACHE}"