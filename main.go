package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

type Employee struct {
	ID         int
	FirstName  string
	LastName   string
	MaxSalary  int
	SalaryDate time.Time
}

func main() {
	dbHost := os.Getenv("MYSQL_HOST")
	if dbHost == "" {
		dbHost = "db"
	}

	db, err := sql.Open("mysql", fmt.Sprintf("root:84c44cacf15cdee4a76f8dab8fb9af44@tcp(%s)/employees?parseTime=true", dbHost))
	if err != nil {
		panic(err)
	}

	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(200)
	db.SetMaxIdleConns(200)

	conn, err := db.Conn(context.Background())
	if err != nil {
		panic(err)
	}
	conn.Close()

	c := NewCache()

	salaries := NewEmployeeRepository(db)
	salariesCached := WrapWithCached(salaries, c)

	rand.Seed(time.Now().UnixNano())

	r := gin.Default()

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	r.GET("/fetch", func(c *gin.Context) {
		limit := rand.Intn(10) + 1

		employees := salaries
		if _, ok := c.GetQuery("cache"); ok {
			employees = salariesCached
		}

		items, err := employees.Fetch(c, limit)
		if err != nil {
			c.AbortWithError(500, err)

			return
		}

		log.Print(items)

		c.JSON(200, items)
	})

	r.Run()
}
