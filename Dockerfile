FROM golang:buster

RUN mkdir /app

WORKDIR /app

ADD go.* ./
RUN go mod download

ADD . .
RUN go build -o /bin/perf

CMD ["/bin/perf"]
